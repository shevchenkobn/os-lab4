// ProcessStat.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

const size_t MODULEENTRY32_SIZE = sizeof MODULEENTRY32;

void printProcess(LPPROCESSENTRY32 processEntry, LPHANDLE snapshot);
void printDllsForProcess(DWORD processId);
void printLastError();

int main()
{
	setlocale(LC_ALL, "Russian");
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);
	PROCESSENTRY32 processEntry;
	processEntry.dwSize = sizeof processEntry;
	printf("Hello! We have such processes:\n\n");
	if (Process32First(snapshot, &processEntry))
	{
		do
		{
			printProcess(&processEntry, &snapshot);
		} while (Process32Next(snapshot, &processEntry));
	}
	CloseHandle(snapshot);
	system("pause");
}

void printProcess(LPPROCESSENTRY32 processEntry, LPHANDLE snapshot)
{
	_tprintf(_T("Name: %s\n"), processEntry->szExeFile);
	_tprintf(_T("Number of used threads: %llu\n"), processEntry->cntThreads);
	_tprintf(_T("List of used dlls:\n"));
	printDllsForProcess(processEntry->th32ProcessID);
	_tprintf(_T("\n\n"));
}

void printDllsForProcess(DWORD processId) {
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, processId);
	if (snapshot == INVALID_HANDLE_VALUE)
	{
		printLastError();
		return;
	}
	MODULEENTRY32 moduleEntry;
	moduleEntry.dwSize = MODULEENTRY32_SIZE;
	if (Module32First(snapshot, &moduleEntry))
	{
		do
		{
			_tprintf(_T("\t%s\n"), moduleEntry.szExePath);
		} while (Module32Next(snapshot, &moduleEntry));
	}
	else
	{
		printLastError();
	}
	CloseHandle(snapshot);
}

void printLastError() {
	DWORD error = GetLastError();
	TCHAR messageBuffer[MAX_PATH];
	size_t size = FormatMessage
	(
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		error,
		0,
		messageBuffer,
		MAX_PATH,
		NULL
	);

	_tprintf(_T("***Error***: #%llu %s"), error, messageBuffer);
}
