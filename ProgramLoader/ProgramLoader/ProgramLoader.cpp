// ProgramLoader.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

void CloseHandles(PROCESS_INFORMATION info) {
	CloseHandle(info.hProcess);
	CloseHandle(info.hThread);
}

FILETIME GetFileTime() {
	SYSTEMTIME sysTime;
	GetSystemTime(&sysTime);

	FILETIME fileTime;
	SystemTimeToFileTime(&sysTime, &fileTime);
	return fileTime;
}

int main()
{
	STARTUPINFO openerStartupInfo = { sizeof(STARTUPINFO) };
	PROCESS_INFORMATION openerProcessInformation;
	STARTUPINFO readerStartupInfo = { sizeof(STARTUPINFO) };
	PROCESS_INFORMATION readerProcessInformation;
	TCHAR openerPath[] = _T("NotepadOpener.exe");

	TCHAR readerPath[256];
	FILETIME currTime = GetFileTime();
	_stprintf(readerPath, _T("%s %u %u"), _T("FileReader.exe"), currTime.dwHighDateTime, currTime.dwLowDateTime);

	if (CREATE_PROCESS(openerPath, &openerStartupInfo, &openerProcessInformation) 
		&& CREATE_SUSPENDED_PROCESS(readerPath, &readerStartupInfo, &readerProcessInformation)) {

		WaitForSingleObject(openerProcessInformation.hProcess, INFINITE);

		ResumeThread(readerProcessInformation.hThread);

		WaitForSingleObject(readerProcessInformation.hProcess, INFINITE);
		CloseHandles(openerProcessInformation);
		CloseHandles(readerProcessInformation);
	}
	else {
		printf("An error has occured");
		return -1;
	}
	system("pause");
    return 0;
}