// stdafx.h: включаемый файл для стандартных системных включаемых файлов
// или включаемых файлов для конкретного проекта, которые часто используются, но
// не часто изменяются
//

#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <Windows.h>

#define CREATE_PROCESS(commandLine, startupInfo, processInformation) \
	CreateProcess(NULL, commandLine, 0, 0, FALSE, 0, 0, 0, startupInfo, processInformation)
#define CREATE_SUSPENDED_PROCESS(commandLine, startupInfo, processInformation) \
	CreateProcess(NULL, commandLine, 0, 0, FALSE, CREATE_SUSPENDED | BELOW_NORMAL_PRIORITY_CLASS, 0, 0, startupInfo, processInformation)
// TODO: Установите здесь ссылки на дополнительные заголовки, требующиеся для программы
