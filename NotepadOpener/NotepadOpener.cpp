// NotepadOpener.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

int main()
{
	STARTUPINFO startupInfo = { sizeof(STARTUPINFO) };
	PROCESS_INFORMATION processInformation;
	TCHAR prog[] = _T("notepad.exe");

	CREATE_PROCESS(prog, &startupInfo, &processInformation);
	WaitForSingleObject(processInformation.hProcess, INFINITE);

	CloseHandle(processInformation.hProcess);
	CloseHandle(processInformation.hThread);
    return 0;
}