// FileReader.cpp: определяет точку входа для консольного приложения
#include "stdafx.h"
#include "FileReader.h"

int _tmain(int argc, wchar_t** argv)
{
	FILETIME* padOpenTime = new FILETIME();
	padOpenTime->dwHighDateTime = (UINT32)_ttoi64(argv[1]);
	padOpenTime->dwLowDateTime = (UINT32)_ttoi64(argv[2]);

	printf("%u %u\n", padOpenTime->dwHighDateTime, padOpenTime->dwLowDateTime);
	printf("Reader is working\n");
	FindRecentFiles(padOpenTime, _T("D:\\FileStorage\\"));
	delete padOpenTime;
	return 0;
}

void FindRecentFiles(FILETIME* padOpenTime, const TCHAR* dirPath) {
	TCHAR* szDir = (TCHAR*)malloc((_tcslen(dirPath) + FILES_WILDCARD_LENGTH + 1) * sizeof(TCHAR));
	_tcscpy(szDir, dirPath);
	_tcscat(szDir, FILES_WILDCARD);

	WIN32_FIND_DATA ffd;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	int ind = 0;

	hFind = FindFirstFile(szDir, &ffd);

	do {
		if (CompareFileTime(&ffd.ftCreationTime, padOpenTime) >= 0) {
			TCHAR* fullPath = (TCHAR*)malloc((_tcslen(ffd.cFileName) + _tcslen(dirPath)) * sizeof(TCHAR));
			_tcscpy(fullPath, dirPath);
			_tcscat(fullPath, ffd.cFileName);

			FileInfo* info = new FileInfo();
			GetAllFileInfo(fullPath, info);

			_tprintf(_T("File: %s\nEncoding: %s\nSize: %d bytes\nRows: %d\n"), ffd.cFileName, EncodingTypeToString(info->encoding), info->size, info->rowCount);

			for (int i = 0; i < info->rowCount; i++) {
				_tprintf(_T("%d row: %d symbols\n"), i + 1, info->rowsLens[i]);
			}
			_tprintf(_T("\n\n"));
			delete info;
		}
	} while (FindNextFile(hFind, &ffd) != 0);

	FindClose(hFind);
	free(szDir);
}

void GetAllFileInfo(TCHAR* fullPath, FileInfo* info) {
	FILE* f = _tfopen(fullPath, _T("r+b"));
	if (f != NULL) {
		int flags = -1;

		fseek(f, 0, SEEK_END);
		int fileLen = ftell(f);
		rewind(f);

		info->size = fileLen;

		char* buf = (char*)malloc(fileLen);
		fread(buf, 1, fileLen, f);

		if (IsTextUnicode(buf, fileLen, &flags)) {
			info->encoding = EncodingType::UnicodeLE;
			CountUnicode(&buf[2], fileLen - 2, LE_NEWLINE, info);
		}
		else {
			if (!memcmp(buf, BE_BOM, 2)) {
				info->encoding = EncodingType::UnicodeBE;
				CountUnicode(&buf[2], fileLen - 2, BE_NEWLINE, info);
			}
			else {
				info->encoding = EncodingType::ASCII;
				CountAscii(buf, fileLen, info);
			}
		}

		fclose(f);
		free(buf);
	}
}

int CountAscii(char* input, int length, FileInfo* info) {
	int count = 0,
		newLineCount = 0;
	std::list<int> indices;
	for (int i = 0; i < length; i++) {
		if (!memcmp(&input[i], ASCII_NEWLINE, 2)) {
			i++;
			indices.push_back(count);
			count = 0;
			newLineCount++;
		}
		else
			count++;
	}
	indices.push_back(count);

	info->rowCount = newLineCount + 1;
	info->rowsLens = new int[info->rowCount];
	std::copy(indices.begin(), indices.end(), info->rowsLens);

	return newLineCount;
}

int CountUnicode(char* input, int length, const char* newLineSymbols, FileInfo* info) {
	int count = 0,
		newLineCount = 0;
	std::list<int> indices;
	for (int i = 0; i < length; i += 2) {
		if (!memcmp(&input[i], newLineSymbols, 4)) {
			i += 2;
			indices.push_back(count);
			count = 0;
			newLineCount++;
		}
		else
			count += 1;
	}
	indices.push_back(count);

	info->rowCount = newLineCount + 1;
	info->rowsLens = new int[info->rowCount];
	std::copy(indices.begin(), indices.end(), info->rowsLens);
	return newLineCount;
}

const TCHAR* EncodingTypeToString(EncodingType type){
	switch (type)
	{
		case UnicodeBE: return _T("UnicodeBE");
		case UnicodeLE: return _T("UnicodeLE");
		case ASCII: return _T("ASCII");
	}
	return _T("ASCII");
}