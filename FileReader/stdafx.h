// stdafx.h: включаемый файл для стандартных системных включаемых файлов
// или включаемых файлов для конкретного проекта, которые часто используются, но
// не часто изменяются
//

#pragma once

#define _CRT_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <list>
#include <Windows.h>

#define LE_BOM "\xFF\xFE"
#define BE_BOM "\xFE\xFF"
#define LE_NEWLINE "\x0D\x00\x0A\x00"
#define BE_NEWLINE "\x00\x0D\x00\x0A"
#define ASCII_NEWLINE "\x0D\x0A"