#pragma once

typedef enum
{
	UnicodeBE = 1,
	UnicodeLE,
	ASCII
} EncodingType;

struct FileInfo
{
	int* rowsLens;
	EncodingType encoding;
	int size;
	int rowCount;
};

const TCHAR FILES_WILDCARD[] = _T("*.txt");
const size_t FILES_WILDCARD_LENGTH = _tcslen(FILES_WILDCARD);

const TCHAR* EncodingTypeToString(EncodingType type);
void FindRecentFiles(FILETIME* padOpenTime, const TCHAR* dirPath);
void GetAllFileInfo(TCHAR* fullPath, FileInfo* info);
int CountAscii(char* input, int length, FileInfo* info);
int CountUnicode(char* input, int length, const char* newLineSymbols, FileInfo* info);